package com.wang.springframework.aop;

import org.aopalliance.aop.Advice;

/**
 * 定义 Advisor 访问者
 *
 * @author wangjubin
 * @date 2024/03/15
 */
public interface Advisor {

    Advice getAdvice();
}
