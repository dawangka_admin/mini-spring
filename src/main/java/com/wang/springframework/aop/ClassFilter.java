package com.wang.springframework.aop;

public interface ClassFilter {

    /**
     * 类匹配
     *
     * @author wangjubin
     */
    boolean matches(Class<?> clazz);
}
