package com.wang.springframework.aop;

import java.lang.reflect.Method;

public interface MethodMatcher {

    /**
     * 方法匹配
     *
     * @author wangjubin
     */
    boolean matches(Method method, Class<?> targetClass);
}
