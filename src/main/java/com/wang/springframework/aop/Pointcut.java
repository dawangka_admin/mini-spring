package com.wang.springframework.aop;

public interface Pointcut {

    /**
     * 获取类筛选器
     *
     * @author wangjubin
     */
    ClassFilter getClassFilter();

    /**
     * 获取方法匹配器
     *
     * @author wangjubin
     */
    MethodMatcher getMethodMatcher();
}
