package com.wang.springframework.aop;

import com.wang.springframework.util.ClassUtils;

public class TargetSource {

    private final Object target;

    public TargetSource(Object target) {
        this.target = target;
    }

    /**
     * 如果此Class对象表示一个类，则返回值是一个数组，
     * 其中包含表示该类直接实现的所有接口的对象。
     * 数组中接口对象的顺序对应于此Class对象所表示的类的声明子句中implements接口名称的顺序
     *
     * @author wangjubin
     */
    public Class<?>[] getTargetClass() {
        Class<?> clazz = this.target.getClass();
        clazz = ClassUtils.isCglibProxyClass(clazz) ? clazz.getSuperclass() : clazz;
        return clazz.getInterfaces();
    }


    public Object getTarget() {
        return this.target;
    }
}
