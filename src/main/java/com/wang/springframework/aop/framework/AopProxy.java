package com.wang.springframework.aop.framework;

/**
 * AOP 代理
 * 定义标准接口，用于获取代理类
 *
 * @author wangjubin
 * @date 2024/03/15
 */
public interface AopProxy {

    Object getProxy();
}
