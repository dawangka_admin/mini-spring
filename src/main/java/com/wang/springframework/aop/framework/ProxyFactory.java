package com.wang.springframework.aop.framework;

import com.wang.springframework.aop.AdvisedSupport;

/**
 * 代理工厂
 *
 * @author wangjubin
 * @date 2024/03/20
 */
public class ProxyFactory {

    private AdvisedSupport advisedSupport;


    public ProxyFactory(AdvisedSupport advisedSupport) {
        this.advisedSupport = advisedSupport;
    }

    public Object getProxy() {
        return createProxy().getProxy();
    }

    //代理工厂主要解决的是关于 JDK 和 Cglib 两种代理的选择问题
    //如果目标类是接口，默认采用 JDK 动态代理
    private AopProxy createProxy() {
        if (advisedSupport.isProxyTargetClass()) {
            return new CglibAopProxy(advisedSupport);
        }
        return new JdkDynamicAopProxy(advisedSupport);
    }
}
