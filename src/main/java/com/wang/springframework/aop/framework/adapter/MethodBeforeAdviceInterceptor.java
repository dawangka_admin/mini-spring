package com.wang.springframework.aop.framework.adapter;

import com.wang.springframework.aop.MethodBeforeAdvice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * 方法拦截器
 *
 * @author wangjubin
 * @date 2024/03/20
 */
public class MethodBeforeAdviceInterceptor implements MethodInterceptor {


    private MethodBeforeAdvice advice;

    public MethodBeforeAdviceInterceptor() {
    }

    public MethodBeforeAdviceInterceptor(MethodBeforeAdvice advice) {
        this.advice = advice;
    }

    public MethodBeforeAdvice getAdvice() {
        return advice;
    }

    public void setAdvice(MethodBeforeAdvice advice) {
        this.advice = advice;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        //在 invoke 方法中调用 advice 中的 before 方法，传入对应的参数信息。
        //例如，传入被拦截的方法、方法参数等信息。
        //调用 advice.before() 方法
        //注意：这里需要使用 MethodBeforeAdvice 接口的 before 方法，而不是 MethodInterceptor 接口的 before 方法。
        this.advice.before(invocation.getMethod(), invocation.getArguments(), invocation.getThis());
        return invocation.proceed();
    }
}
