package com.wang.springframework.beans.factory;

/**
 * 标记类接口，实现该接口可以被spring的容器感知
 *
 * @author wangjubin
 * @date 2024/03/04
 */
public interface Aware {
}
