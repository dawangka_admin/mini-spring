package com.wang.springframework.beans.factory;


/**
 * BeanClassLoader感知
 * 实现此接口，即可感知到所属的classLoader
 *
 * @author wangjubin
 * @date 2024/03/04
 */
public interface BeanClassLoaderAWare extends Aware {

    void setBeanClassLoader(ClassLoader classLoader);
}
