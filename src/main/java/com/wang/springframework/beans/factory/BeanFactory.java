package com.wang.springframework.beans.factory;

import com.wang.springframework.beans.BeansException;

public interface BeanFactory {

    /**
     * 获取 Bean
     *
     * @param beanName Bean 名称
     * @return {@link Object}
     */
    Object getBean(String beanName);

    Object getBean(String beanName, Object... args);

    <T> T getBean(String beanName, Class<T> requiredType);

    <T> T getBean(Class<T> requiredType) throws BeansException;

}
