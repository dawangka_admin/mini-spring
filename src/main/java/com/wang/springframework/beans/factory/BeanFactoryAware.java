package com.wang.springframework.beans.factory;

import com.wang.springframework.beans.BeansException;

/**
 * Bean 工厂感知
 * 实现此接口，既能感知到所属的BeanFactory
 *
 * @author wangjubin
 * @date 2024/03/04
 */
public interface BeanFactoryAware extends Aware {

    void setBeanFactory(BeanFactory beanFactory) throws BeansException;
}
