package com.wang.springframework.beans.factory;

/**
 * BeanName感知
 * 实现此接口，即可感知到所属的 BeanName
 *
 * @author wangjubin
 * @date 2024/03/04
 */
public interface BeanNameAware extends Aware {

    void setBeanName(String beanName);
}
