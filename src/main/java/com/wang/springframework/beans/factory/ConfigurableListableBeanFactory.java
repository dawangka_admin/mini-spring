package com.wang.springframework.beans.factory;

import com.wang.springframework.beans.BeansException;
import com.wang.springframework.beans.factory.config.AutowireCapableBeanFactory;
import com.wang.springframework.beans.factory.config.BeanDefinition;
import com.wang.springframework.beans.factory.config.ConfigurableBeanFactory;

public interface ConfigurableListableBeanFactory
        extends ListableBeanFactory, AutowireCapableBeanFactory, ConfigurableBeanFactory {

    /**
     * Return the registered BeanDefinition for the specified bean, allowing access
     * to its property values and constructor argument value (which can be
     * modified during bean factory post-processing).
     */
    BeanDefinition getBeanDefinition(String beanName);


    /**
     * Ensure that all non-lazy-init singletons are instantiated, also considering
     * Typically invoked at the end of factory setup, if desired.
     * Note: This may have left the factory with some beans already initialized!
     */
    void preInstantiateSingletons() throws BeansException;
}
