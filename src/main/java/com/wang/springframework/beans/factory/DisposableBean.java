package com.wang.springframework.beans.factory;

public interface DisposableBean {

    void destroy() throws Exception;
}
