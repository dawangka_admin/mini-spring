package com.wang.springframework.beans.factory;

import com.wang.springframework.beans.BeansException;

import java.util.Map;

public interface ListableBeanFactory extends BeanFactory {


    /**
     * 按照类型返回Bean实例
     *
     * @param type 类型
     * @return {@link Map}<{@link String}, {@link T}>
     * @throws BeansException Beans 异常
     */

    <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException;

    /**
     * 返回注册表中Bean名称
     *
     * @return {@link String[]}
     */
    String[] getBeanDefinitionNames();


}
