package com.wang.springframework.beans.factory;

import com.wang.springframework.beans.BeansException;

/**
 * 定义一个可以返回对象的工厂
 *
 * @author wangjubin
 * @date 2024/04/08
 */
public interface ObjectFactory<T> {

    T getObject() throws BeansException;
}
