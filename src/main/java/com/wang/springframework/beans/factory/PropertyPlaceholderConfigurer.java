package com.wang.springframework.beans.factory;

import com.wang.springframework.beans.BeansException;
import com.wang.springframework.beans.PropertyValue;
import com.wang.springframework.beans.PropertyValues;
import com.wang.springframework.beans.factory.config.BeanDefinition;
import com.wang.springframework.beans.factory.config.BeanFactoryPostProcessor;
import com.wang.springframework.core.io.DefaultResourceLoader;
import com.wang.springframework.core.io.Resource;
import com.wang.springframework.util.StringValueResolver;

import java.io.IOException;
import java.util.Properties;


/**
 * 处理占位符配置
 *
 * @author wangjubin
 * @date 2024/03/21
 */
public class PropertyPlaceholderConfigurer implements BeanFactoryPostProcessor {


    public static final String DEFAULT_PLACEHOLDER_PREFIX = "${";
    public static final String DEFAULT_PLACEHOLDER_SUFFIX = "}";

    private String location;


    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        try {
            DefaultResourceLoader resourceLoader = new DefaultResourceLoader();
            Resource resource = resourceLoader.getResource(location);
            //加载Properties配置文件
            Properties properties = new Properties();
            properties.load(resource.getInputStream());
            String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
            for (String beanName : beanDefinitionNames) {
                BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
                PropertyValues propertyValues = beanDefinition.getPropertyValues();
                //获取XML中的配置信息
                for (PropertyValue propertyValue : propertyValues.getPropertyValues()) {
                    Object value = propertyValue.getValue();
                    if (!(value instanceof String strVal)) {
                        continue;
                    }
                    strVal = resolvePlaceholder(strVal, properties);
                    propertyValues.addPropertyValue(new PropertyValue(propertyValue.getName(), strVal));
                }
            }

            // 向容器中添加字符串解析器，供解析@Value注解使用
            StringValueResolver valueResolver = new PlaceholderResolvingStringValueResolver(properties);
            beanFactory.addEmbeddedValueResolver(valueResolver);
        } catch (IOException e) {
            throw new BeansException("Could not load properties", e);
        }

    }


    private String resolvePlaceholder(String value, Properties properties) {
        String strVal = value;
        int startIdx = strVal.indexOf(DEFAULT_PLACEHOLDER_PREFIX);
        int stopIdx = strVal.indexOf(DEFAULT_PLACEHOLDER_SUFFIX);
        //判断是否包含${}占位符
        if (startIdx != -1 && stopIdx != -1 && startIdx < stopIdx) {
            String propVal = strVal.substring(startIdx + 2, stopIdx);
            String holder = DEFAULT_PLACEHOLDER_PREFIX + propVal + DEFAULT_PLACEHOLDER_SUFFIX;
            //将${}占位符替换为配置文件中的值
            if (properties.containsKey(propVal)) {
                strVal = strVal.replace(holder, properties.getProperty(propVal));
            }
        }
        return strVal;
    }


    private class PlaceholderResolvingStringValueResolver implements StringValueResolver {

        private final Properties properties;

        private PlaceholderResolvingStringValueResolver(Properties properties) {
            this.properties = properties;
        }

        @Override
        public String resolveStringValue(String strVal) {
            return resolvePlaceholder(strVal, properties);
        }
    }
}
