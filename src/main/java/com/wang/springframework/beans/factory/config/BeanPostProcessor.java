package com.wang.springframework.beans.factory.config;

import com.wang.springframework.beans.BeansException;

public interface BeanPostProcessor {

    /**
     * 在bean的初始化方法调用之前，对bean进行增强处理
     *
     * @author wangjubin
     */
    Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException;

    /**
     * 在bean的初始化方法调用之后，对bean进行增强处理
     *
     * @author wangjubin
     */
    Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException;
}
