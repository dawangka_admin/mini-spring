package com.wang.springframework.beans.factory.config;

/**
 * bean引用对象
 *
 * @author wangjubin
 * @date 2024/02/06
 */
public class BeanReference {

    private String beanName;

    public BeanReference(String beanName) {
        this.beanName = beanName;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
