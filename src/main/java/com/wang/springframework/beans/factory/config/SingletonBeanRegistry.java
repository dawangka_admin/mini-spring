package com.wang.springframework.beans.factory.config;

public interface SingletonBeanRegistry {

    /**
     * 获取单例
     *
     * @param beanName Bean 名称
     * @return {@link Object}
     */
    Object getSingleton(String beanName);


    void registerSingleton(String beanName, Object singletonObject);
}
