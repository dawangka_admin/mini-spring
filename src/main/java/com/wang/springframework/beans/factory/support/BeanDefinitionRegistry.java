package com.wang.springframework.beans.factory.support;

import com.wang.springframework.beans.factory.config.BeanDefinition;

/**
 * Bean 定义注册表
 *
 * @author wangjubin
 * @date 2024/02/03
 */
public interface BeanDefinitionRegistry {

    /**
     * 注册beanDefinition
     *
     * @param beanName       Bean 名称
     * @param beanDefinition Bean 定义
     */
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition);

    boolean containsBeanDefinition(String beanName);
}
