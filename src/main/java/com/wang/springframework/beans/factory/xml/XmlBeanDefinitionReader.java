package com.wang.springframework.beans.factory.xml;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import com.wang.springframework.beans.BeansException;
import com.wang.springframework.beans.PropertyValue;
import com.wang.springframework.beans.factory.config.BeanDefinition;
import com.wang.springframework.beans.factory.config.BeanReference;
import com.wang.springframework.beans.factory.support.AbstractBeanDefinitionReader;
import com.wang.springframework.beans.factory.support.BeanDefinitionRegistry;
import com.wang.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import com.wang.springframework.core.io.Resource;
import com.wang.springframework.core.io.ResourceLoader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;

public class XmlBeanDefinitionReader extends AbstractBeanDefinitionReader {
    public XmlBeanDefinitionReader(BeanDefinitionRegistry registry) {
        super(registry);
    }

    public XmlBeanDefinitionReader(BeanDefinitionRegistry registry, ResourceLoader resourceLoader) {
        super(registry, resourceLoader);
    }

    @Override
    public void loadBeanDefinitions(Resource resource) throws BeansException {
        try (InputStream inputStream = resource.getInputStream()) {
            // 解析 xml 文件
            doLoadBeanDefinitions(inputStream);
        } catch (Exception e) {
            throw new BeansException("IOException parsing XML document from " + resource, e);
        }
    }


    @Override
    public void loadBeanDefinitions(Resource... resources) throws BeansException {
        for (Resource resource : resources) {
            loadBeanDefinitions(resource);
        }
    }

    @Override
    public void loadBeanDefinitions(String... locations) throws BeansException {
        for (String location : locations) {
            loadBeanDefinitions(location);
        }
    }


    @Override
    public void loadBeanDefinitions(String location) throws BeansException {
        ResourceLoader resourceLoader = getResourceLoader();
        Resource resource = resourceLoader.getResource(location);
        loadBeanDefinitions(resource);
    }

    private void doLoadBeanDefinitions(InputStream inputStream) throws ClassNotFoundException {
        Document doc = XmlUtil.readXML(inputStream);
        Element root = doc.getDocumentElement();
        NodeList childNodes = root.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            if (!(childNodes.item(i) instanceof Element bean)) {
                continue;
            }

            if ("component-scan".equals(childNodes.item(i).getNodeName())) {
                String basePackage = bean.getAttribute("base-package");
                if (StrUtil.isNotEmpty(basePackage)) {
                    scanPackage(basePackage);
                }
            }

            if ("bean".equals(childNodes.item(i).getNodeName())) {
                //解析标签
                String id = bean.getAttribute("id");
                String name = bean.getAttribute("name");
                String className = bean.getAttribute("class");
                //获取init-method、destroy-method
                String initMethodName = bean.getAttribute("init-method");
                String destroyMethodName = bean.getAttribute("destroy-method");

                String scope = bean.getAttribute("scope");


                Class<?> clazz = Class.forName(className);
                String beanName = StrUtil.isNotEmpty(id) ? id : name;
                if (StrUtil.isEmpty(beanName)) {
                    beanName = StrUtil.lowerFirst(clazz.getSimpleName());
                }
                BeanDefinition beanDefinition = new BeanDefinition(clazz);

                beanDefinition.setInitMethodName(initMethodName);
                beanDefinition.setDestroyMethodName(destroyMethodName);

                if (StrUtil.isNotEmpty(scope)) {
                    beanDefinition.setScope(scope);
                }

                for (int j = 0; j < bean.getChildNodes().getLength(); j++) {
                    if (!(bean.getChildNodes().item(j) instanceof Element property)) {
                        continue;
                    }
                    if (!"property".equals(bean.getChildNodes().item(j).getNodeName())) {
                        continue;
                    }
                    String attrName = property.getAttribute("name");
                    String attrValue = property.getAttribute("value");
                    String attrRef = property.getAttribute("ref");
                    Object value = StrUtil.isEmpty(attrValue) ? new BeanReference(attrRef) : attrValue;
                    PropertyValue propertyValue = new PropertyValue(attrName, value);
                    beanDefinition.getPropertyValues().addPropertyValue(propertyValue);

                }
                if (getRegistry().containsBeanDefinition(beanName)) {
                    throw new BeansException("Duplicate beanName[" + beanName + "] is not allowed");
                }
                getRegistry().registerBeanDefinition(beanName, beanDefinition);
            }
        }

    }

    private void scanPackage(String scanPath) {
        String[] basePackages = StrUtil.splitToArray(scanPath, ',');
        ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(getRegistry());
        scanner.doScan(basePackages);
    }
}
