package com.wang.springframework.context;

import com.wang.springframework.beans.factory.HierarchicalBeanFactory;
import com.wang.springframework.beans.factory.ListableBeanFactory;
import com.wang.springframework.core.io.ResourceLoader;

public interface ApplicationContext extends ListableBeanFactory, HierarchicalBeanFactory, ResourceLoader, ApplicationEventPublisher {
}
