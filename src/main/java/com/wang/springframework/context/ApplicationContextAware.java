package com.wang.springframework.context;

import com.wang.springframework.beans.factory.Aware;

/**
 * Application感知
 *
 * @author wangjubin
 * @date 2024/03/04
 */
public interface ApplicationContextAware extends Aware {

    void setApplicationContext(ApplicationContext applicationContext);
}
