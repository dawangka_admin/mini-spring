package com.wang.springframework.context;

/**
 * 事件发布者
 *
 * @author wangjubin
 * @date 2024/03/08
 */
public interface ApplicationEventPublisher {

    void publishEvent(ApplicationEvent event);
}
