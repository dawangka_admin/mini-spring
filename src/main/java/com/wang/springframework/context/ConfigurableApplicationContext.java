package com.wang.springframework.context;

import com.wang.springframework.beans.BeansException;

public interface ConfigurableApplicationContext extends ApplicationContext {

    /**
     * 刷新容器
     */
    void refresh() throws BeansException;

    /**
     * 注册钩子函数
     */
    void registerShutdownHook();

    void close();


}
