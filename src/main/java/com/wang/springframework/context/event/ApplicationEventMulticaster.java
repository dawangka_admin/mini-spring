package com.wang.springframework.context.event;

import com.wang.springframework.context.ApplicationEvent;
import com.wang.springframework.context.ApplicationListener;

/**
 * 事件广播器
 *
 * @author wangjubin
 * @date 2024/03/08
 */
public interface ApplicationEventMulticaster {

    void addApplicationListener(ApplicationListener<?> listener);

    void removeApplicationListener(ApplicationListener<?> listener);


    void multicastEvent(ApplicationEvent event);
}
