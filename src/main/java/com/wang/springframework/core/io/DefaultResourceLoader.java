package com.wang.springframework.core.io;

import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;

import java.net.MalformedURLException;
import java.net.URL;

public class DefaultResourceLoader implements ResourceLoader {


    @Override
    public Resource getResource(String location) {
        Assert.notNull(location, "Location must not be null");
        if (location.startsWith(ResourceLoader.CLASSPATH)) {
            return new ClassPathResource(location.substring(ResourceLoader.CLASSPATH.length()));
        } else {
            try {
                URL url = new URL(location);
                return ResourceUtils.isFileURL(url) ? new FileSystemResource(url.getFile()) : new UrlResource(url);
            } catch (MalformedURLException e) {
                return new ClassPathResource(location);
            }
        }

    }
}
