package com.wang.springframework.core.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * 资源加载接口
 *
 * @author wangjubin
 * @date 2024/02/06
 */
public interface Resource {

    InputStream getInputStream() throws IOException;
}
