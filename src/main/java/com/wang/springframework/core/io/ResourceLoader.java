package com.wang.springframework.core.io;

/**
 * 资源加载程序
 *
 * <p>
 * 按照资源加载的不同方式，资源加载器将这些方式集中到统一的类服务下进行处理，外部用户只需要传递资源地址即可，简化使用。
 * </p>
 *
 * @author wangjubin
 * @date 2024/02/06
 */
public interface ResourceLoader {

    String CLASSPATH = "classpath:";

    Resource getResource(String location);
}
