package com.wang.springframework.util;

/**
 * 字符串值解析器
 *
 * @author wangjubin
 * @date 2024/03/21
 */
public interface StringValueResolver {

    String resolveStringValue(String strVal);
}
