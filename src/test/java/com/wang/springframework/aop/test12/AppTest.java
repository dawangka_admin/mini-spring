package com.wang.springframework.aop.test12;

import com.wang.springframework.aop.AdvisedSupport;
import com.wang.springframework.aop.TargetSource;
import com.wang.springframework.aop.aspectj.AspectJExpressionPointcut;
import com.wang.springframework.aop.framework.CglibAopProxy;
import com.wang.springframework.aop.framework.JdkDynamicAopProxy;
import com.wang.springframework.aop.test12.bean.IUserService;
import com.wang.springframework.aop.test12.bean.UserService;
import com.wang.springframework.aop.test12.bean.UserServiceInterceptor;
import org.junit.Test;


public class AppTest {

    @Test
    public void test_dynamic() {
        IUserService userService = new UserService();
        System.out.println(userService.queryUserInfo());
        System.out.println("------------------------------");
        AdvisedSupport advisedSupport = new AdvisedSupport();
        advisedSupport.setTargetSource(new TargetSource(userService));
        advisedSupport.setMethodMatcher(new AspectJExpressionPointcut("execution(* com.wang.springframework.aop.test12.bean.IUserService.*(..))"));
        advisedSupport.setMethodInterceptor(new UserServiceInterceptor());

//        //JdkDynamicAopProxy
        IUserService proxy_jdk = (IUserService) new JdkDynamicAopProxy(advisedSupport).getProxy();
        System.out.println(proxy_jdk.queryUserInfo());
        System.out.println("------------------------------");
        //CglibAopProxy
        IUserService proxy_cglib = (IUserService) new CglibAopProxy(advisedSupport).getProxy();
        System.out.println(proxy_cglib.queryUserInfo());
    }

}
