package com.wang.springframework.aop.test12.bean;

public interface IUserService {

    String queryUserInfo();

    String register(String userName);
}
