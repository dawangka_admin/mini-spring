package com.wang.springframework.aop.test13;

import com.wang.springframework.aop.test13.bean.IUserService;
import com.wang.springframework.context.support.ClassPathXmlApplicationContext;
import org.junit.Test;


public class AppTest {

    @Test
    public void test_aop() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:aop/13/spring.xml");
        IUserService userService = (IUserService) applicationContext.getBean("userService");
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

}
