package com.wang.springframework.aop.test13.bean;

public interface IUserService {

    String queryUserInfo();

    String register(String userName);
}
