package com.wang.springframework.aop.test13.bean;

public class UserService implements IUserService {
    @Override
    public String queryUserInfo() {
        return "查询到用户信息";
    }

    @Override
    public String register(String userName) {
        return "注册用户：" + userName + " success！";
    }


}
