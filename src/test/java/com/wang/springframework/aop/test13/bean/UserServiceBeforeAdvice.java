package com.wang.springframework.aop.test13.bean;

import cn.hutool.core.collection.CollectionUtil;
import com.wang.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class UserServiceBeforeAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("前置通知 - Begin By AOP");
        System.out.println("方法名称：" + method);
        System.out.println("参数列表：" + CollectionUtil.toList(args));
        System.out.println("目标对象：" + target);
        System.out.println("前置通知 - End\r\n");
    }
}
