package com.wang.springframework.aop.test14;

import com.wang.springframework.aop.test14.bean.IUserService;
import com.wang.springframework.context.support.ClassPathXmlApplicationContext;
import org.junit.Test;


public class AppTest {

    /**
     * 测试占位符
     */
    @Test
    public void test_property() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:aop/14/spring-property.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

    @Test
    public void test_scan() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:aop/14/spring-scan.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

}
