package com.wang.springframework.aop.test14.bean;

public interface IUserService {

    String queryUserInfo();

    String register(String userName);
}
