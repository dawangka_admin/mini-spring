package com.wang.springframework.aop.test14.bean;

import com.wang.springframework.stereotype.Component;

@Component("userService")
public class UserService implements IUserService {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String queryUserInfo() {
        return "查询到用户信息" + token;
    }

    @Override
    public String register(String userName) {
        return "注册用户" + userName;
    }


}
