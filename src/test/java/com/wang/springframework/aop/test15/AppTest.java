package com.wang.springframework.aop.test15;

import com.wang.springframework.aop.test15.bean.IUserService;
import com.wang.springframework.context.support.ClassPathXmlApplicationContext;
import org.junit.Test;


public class AppTest {


    @Test
    public void test_scan() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:aop/15/spring.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

}
