package com.wang.springframework.aop.test15.bean;

public interface IUserService {

    String queryUserInfo();

    String register(String userName);
}
