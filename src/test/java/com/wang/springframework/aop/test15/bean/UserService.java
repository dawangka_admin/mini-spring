package com.wang.springframework.aop.test15.bean;

import com.wang.springframework.beans.factory.annotation.Autowired;
import com.wang.springframework.beans.factory.annotation.Value;
import com.wang.springframework.stereotype.Component;

@Component("userService")
public class UserService implements IUserService {

    @Value("${token}")
    private String token;

    @Autowired
    private UserDao userDao;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String queryUserInfo() {
        return userDao.queryUserName("10002") + "，token:" + token;
    }

    @Override
    public String register(String userName) {
        return "注册用户" + userName;
    }


}
