package com.wang.springframework.ioc.test;

import com.wang.springframework.beans.PropertyValue;
import com.wang.springframework.beans.PropertyValues;
import com.wang.springframework.beans.factory.config.BeanDefinition;
import com.wang.springframework.beans.factory.config.BeanReference;
import com.wang.springframework.beans.factory.support.DefaultListableBeanFactory;
import com.wang.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import com.wang.springframework.context.support.ClassPathXmlApplicationContext;
import com.wang.springframework.ioc.test.bean.UserDao;
import com.wang.springframework.ioc.test.bean.UserService;
import com.wang.springframework.ioc.test.common.MyBeanFactoryPostProcessor;
import com.wang.springframework.ioc.test.common.MyBeanPostProcessor;
import com.wang.springframework.ioc.test.event.CustomEvent;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {


    @Test
    public void testApp() {
        //1、初始化beanFactory
        DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
        //2. 添加userDao
        defaultListableBeanFactory.registerBeanDefinition("userDao", new BeanDefinition(UserDao.class));
        // 3. UserService 设置属性[uId、userDao]
        PropertyValues propertyValues = new PropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("uId", "10003"));
        propertyValues.addPropertyValue(new PropertyValue("userDao", new BeanReference("userDao")));
        //4、注册userService
        defaultListableBeanFactory.registerBeanDefinition("userService", new BeanDefinition(UserService.class, propertyValues));
        // defaultListableBeanFactory.setInstantiationStrategy(new CglibSubclassingInstantiationStrategy());
        //获取bean
//        UserService userService = (UserService) defaultListableBeanFactory.
//                getBean("userService");
//
//        userService.queryUserInfo();
        //获取bean
        UserService userService_singleton = (UserService) defaultListableBeanFactory.
                getBean("userService");
        System.out.println(userService_singleton.queryUserInfo());

    }

    @Test
    public void testXml() {
        //1、初始化beanFactory
        DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
        xmlBeanDefinitionReader.loadBeanDefinitions("classpath:spring.xml");
        UserService userService_singleton = defaultListableBeanFactory.
                getBean("userService", UserService.class);
        System.out.println(userService_singleton.queryUserInfo());

    }

    /**
     * 测试 Bean 工厂后处理器和 Bean 后处理器
     * 不使用应用上下文
     */
    @Test
    public void test_BeanFactoryPostProcessorAndBeanPostProcessor() {
        //1、初始化beanFactory
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        //2、读取配置文件&注册beanDefinition
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        reader.loadBeanDefinitions("classpath:ioc/spring.xml");
        //3、beanDefinition加载完成&Bean实例化之前，修改beanDefinition属性值
        MyBeanFactoryPostProcessor myBeanFactoryPostProcessor = new MyBeanFactoryPostProcessor();
        myBeanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
        // 4、注册BeanPostProcessor，并且等待Bean实例化之后，修改bean属性信息
        MyBeanPostProcessor myBeanPostProcessor = new MyBeanPostProcessor();
        beanFactory.addBeanPostProcessor(myBeanPostProcessor);
        //4.获取bean对象调用方法
        UserService userService = beanFactory.getBean("userService", UserService.class);
        System.out.println(userService.queryUserInfo());


    }

    @Test
    public void testApplicationContext() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:ioc/spring.xml");
        //注册钩子函数，虚拟机关闭时执行钩子函数
        applicationContext.registerShutdownHook();

        UserService userService = applicationContext.getBean("userService", UserService.class);
        System.out.println(userService.queryUserInfo());
        System.out.println("beanFactoryAware:" + userService.getBeanFactory());
        System.out.println("ApplicationContextAware:" + userService.getApplicationContext());


    }

    @Test
    public void test_event() {
        ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("classpath:ioc/11/spring.xml");
        //注册钩子函数，虚拟机关闭时执行钩子函数
        applicationContext.registerShutdownHook();

        applicationContext.publishEvent(new CustomEvent(applicationContext, 1019129009086763L, "成功了！"));


    }

}
