package com.wang.springframework.ioc.test;

import cn.hutool.core.io.IoUtil;
import com.wang.springframework.core.io.DefaultResourceLoader;
import com.wang.springframework.core.io.Resource;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Unit test for simple App.
 */
public class ResouceLoaderTest {


    @Test
    public void testClasspath() throws IOException {
        DefaultResourceLoader
                resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource("classpath:ioc/spring.xml");
        InputStream inputStream = resource.getInputStream();
        String content = IoUtil.readUtf8(inputStream);
        System.out.println(content);


    }

    @Test
    public void testURL() throws IOException {
        DefaultResourceLoader
                resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource("https://gitee.com/dawangka_admin/mini-spring/raw/master/src/main/resources/spring.xml");
        InputStream inputStream = resource.getInputStream();
        String content = IoUtil.readUtf8(inputStream);
        System.out.println(content);


    }

}
