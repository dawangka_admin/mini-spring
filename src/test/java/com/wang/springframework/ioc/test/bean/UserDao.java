package com.wang.springframework.ioc.test.bean;

import java.util.HashMap;
import java.util.Map;

public class UserDao {

    private static Map<String, Object> hashMap = new HashMap<>();

    public void initDataMethod() {
        System.out.println("【UserDao】 执行init-method");
        hashMap.put("10001", "小傅哥");
        hashMap.put("10002", "八哥");
        hashMap.put("10003", "狗蛋");
    }

    public void destroyDataMethod() {
        System.out.println("【UserDao】 执行destroy-method");
        hashMap.clear();

    }

    public String queryUserName(String uId) {
        return "【UserDao】查询用户ID：" + uId + "，对应的用户名：" + hashMap.get(uId);
    }


}
