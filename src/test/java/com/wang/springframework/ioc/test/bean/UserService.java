package com.wang.springframework.ioc.test.bean;

import com.wang.springframework.beans.BeansException;
import com.wang.springframework.beans.factory.*;
import com.wang.springframework.context.ApplicationContext;
import com.wang.springframework.context.ApplicationContextAware;

public class UserService implements InitializingBean, DisposableBean,
        BeanNameAware, BeanClassLoaderAWare, ApplicationContextAware, BeanFactoryAware {


    private ApplicationContext applicationContext;

    private BeanFactory beanFactory;

    private String uId;

    private String company;

    private String location;


    private UserDao userDao;

    public String getUid() {
        return uId;
    }

    public void setUid(String uId) {
        this.uId = uId;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }


    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String queryUserInfo() {
        return userDao.queryUserName(uId) + ", 公司：" + company + ", 地点" + location;
    }

    @Override
    public void destroy() {
        System.out.println("【userService】执行DisposableBean接口的destroy方法");
    }

    @Override
    public void afterPropertiesSet() throws BeansException {
        System.out.println("【userService】执行InitializingBean接口的afterPropertiesSet方法");
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        System.out.println("classLoader:" + classLoader);
    }

    @Override
    public void setBeanName(String beanName) {
        System.out.println("Bean Name is :" + beanName);
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
 