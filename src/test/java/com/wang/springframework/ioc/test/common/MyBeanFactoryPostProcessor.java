package com.wang.springframework.ioc.test.common;

import com.wang.springframework.beans.BeansException;
import com.wang.springframework.beans.PropertyValue;
import com.wang.springframework.beans.PropertyValues;
import com.wang.springframework.beans.factory.ConfigurableListableBeanFactory;
import com.wang.springframework.beans.factory.config.BeanDefinition;
import com.wang.springframework.beans.factory.config.BeanFactoryPostProcessor;

/**
 * 允许在 Bean 对象注册后但未实例化之前，对 Bean 的定义信息 BeanDefinition 执行修改操作
 * 在方法中可以对beanFactory进行操作
 * 例如：修改bean的定义、添加bean等
 * 注意：postProcessBeanFactory方法在所有bean定义信息被加载后，但在bean实例化之前执行
 * 通常用于对bean的定义进行修改
 * 例如：修改bean的scope、修改bean的lazyInit等
 *
 * @author wangjubin
 * @date 2024/02/28
 */
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {


    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("userService");
        //修改bean的定义信息
        PropertyValues propertyValues = beanDefinition.getPropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("company", "对 Bean 的定义信息 BeanDefinition 执行修改操作"));

    }
}
