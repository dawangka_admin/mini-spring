package com.wang.springframework.ioc.test.common;

import com.wang.springframework.beans.BeansException;
import com.wang.springframework.beans.factory.config.BeanPostProcessor;
import com.wang.springframework.ioc.test.bean.UserService;

/**
 * 是在 Bean 对象实例化之后修改 Bean 对象，也可以替换 Bean 对象
 *
 * @author wangjubin
 * @date 2024/02/28
 */
public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof UserService userService) {
            userService.setLocation("对象实例化之后修改 Bean 对象");
        }
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return null;
    }
}
