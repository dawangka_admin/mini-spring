package com.wang.springframework.ioc.test10.bean;

public interface IUserDao {

    String queryUserName(String uId);
}
