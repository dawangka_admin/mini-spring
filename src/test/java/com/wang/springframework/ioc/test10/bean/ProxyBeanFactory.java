package com.wang.springframework.ioc.test10.bean;

import com.wang.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ProxyBeanFactory implements FactoryBean<IUserDao> {
    @Override
    public IUserDao getObject() throws Exception {
        // 创建代理对象
        InvocationHandler invocationHandler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Map<String, String> hashMap = new HashMap<>();
                hashMap.put("10001", "小傅哥");
                hashMap.put("10002", "八杯水");
                hashMap.put("10003", "阿毛");
                return "你被代理了 " + method.getName() + "：" + hashMap.get(args[0].toString());
            }
        };
        return (IUserDao) Proxy.newProxyInstance(ProxyBeanFactory.class.getClassLoader(), new Class[]{IUserDao.class}, invocationHandler);
    }

    @Override
    public Class<?> getObjectType() {
        return IUserDao.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
